const {Elm} = require("./src/Main.elm");
const PagesInit = require("elm-pages");

PagesInit({
  mainElmModule: Elm.Main
});
