.PHONY: all
all: build

.PHONY: init
init:
	# npm init
	# npm install --save-dev elm elm-pages
	npx elm install elm-community/maybe-extra
	npx elm install elm/url
	npx elm install mdgriffith/elm-markup
	npx elm install mdgriffith/elm-ui
	npx elm install elm/json
	npx elm install avh4/elm-color
	npx elm install elm-community/result-extra
	npx elm install elm-community/list-extra
	npx elm install elm/http
	npx elm install elm-explorations/markdown
	npx elm install dillonkearns/elm-pages

.PHONY: clean

clean:
	rm -rf node_modules 

.PHONY: build
build:
	npx elm-pages build

.PHONY: develop
develop:
	npx elm-pages develop
