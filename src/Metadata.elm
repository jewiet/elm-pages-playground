module Metadata exposing (Metadata(..), PageMetadata, decoder)

import Dict exposing (Dict)
import Element exposing (Element)
import Html exposing (Html)
import Json.Decode as Decode exposing (Decoder)
import Pages
import Pages.Document


type Metadata
    = Page PageMetadata


type alias PageMetadata =
    { title : String }


decoder =
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\pageType ->
                case pageType of
                    "page" ->
                        Decode.map (\title -> { title = title })
                            (Decode.field
                                "title"
                                Decode.string
                            )

                    _ ->
                        Decode.fail <| "Unexpected page type" ++ pageType
            )
