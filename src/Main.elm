module Main exposing (main)

import Color
import Element exposing (Element)
import Html exposing (Html)
import Markdown
import Metadata exposing (Metadata)
import Pages exposing (images, pages)
import Pages.Document
import Pages.Manifest as Manifest
import Pages.Manifest.Category


main =
    Pages.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , documents = [ markdownDocument ]
        , head = head
        , manifest = manifest
        , canonicalSiteUrl = "https://jewiet.gitlab.io/elm-pages-playground/"
        }


markdownDocument =
    Pages.Document.parser
        { extension = "md"
        , metadata = Metadata.decoder
        , body =
            \markdownBody ->
                Html.div [] [ Markdown.toHtml [] markdownBody ]
                    |> Element.html
                    |> List.singleton
                    |> Element.paragraph [ Element.width Element.fill ]
                    |> Ok
        }


type alias Model =
    ()


type alias Msg =
    ()


type alias Rendered =
    Element Msg


type alias Metadata =
    ()


init : ( Model, Cmd Msg )
init =
    ( (), Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        () ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


head _ =
    []


manifest =
    { backgroundColor = Just Color.white
    , categories = [ Pages.Manifest.Category.education ]
    , displayMode = Manifest.Standalone
    , orientation = Manifest.Portrait
    , description = "A playground for elm-pages"
    , iarcRatingId = Nothing
    , name = "elm-pages-playground"
    , themeColor = Just Color.white
    , startUrl = pages.index
    , shortName = Just "playground"
    , sourceIcon = images.squirrel
    }


view _ site page =
    { title = "HIHO"
    , body =
        "what is zijn naam"
            |> Element.text
            |> Element.layout []
    }
